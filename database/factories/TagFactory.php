<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Tag::class, function (Faker $faker) {
    $twoWords = rand(0,1000) % 2 == 0;

    if ($twoWords) {
        $word = str_replace([' ','.'], ['',''], strtolower($faker->sentence(2))). '_' . $faker->word;
    } else {
        $word = str_replace([' ','.'], ['',''], strtolower($faker->sentence(3)));
    }

    return [
        'tag' => $word,
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ];
});
