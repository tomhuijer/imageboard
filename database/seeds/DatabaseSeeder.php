<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $tags = factory(App\Tag::class, 50)->create();
        $posts = factory(App\Post::class, 1000)->create();

        foreach($posts as $post) {
            foreach ($tags->shuffle()->slice(0, 15) as $tag) {
                \App\PostTag::create([
                    'post_id' => $post->id,
                    'tag_id' => $tag->id
                ]);
            }
        }
    }
}
