@extends('master')

@section('title', 'Posts')

@section('content')
    <div class="row">
        <nav class="col-md-2">
            <ul class="post-tags">
                @foreach ($post->tags as $tag)
                    <li>
                        <a href="{{ route('tags.destroy', [$post->id, $tag->id]) }}" class="text-danger" onclick="return confirm('Remove tag {{ $tag->tag }}?');">&times;</a>
                        <a href="{{ route('posts.index', [$tag->tag, 1]) }}">{{ $tag->tag }}</a>
                        <small class="text-muted">{{ $tag->count }}</small>
                    </li>
                @endforeach
            </ul>

            <form id="form-tags-add" class="form-inline mt-3" method="post" action="{{ route('tags.create', $post->id) }}" >
                {{ csrf_field() }}
                <input id="input-tags" class="mr-2" type="text" placeholder="Add tags" aria-label="Add tags" name="tags">
                <button class="btn btn-outline-success" type="submit">Add</button>
            </form>
        </nav>

        <main role="main" class="col-md-10 text-center">
            <img src="{{ $post->image() }}" class="post" />
        </main>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">

        $('#input-tags').selectize({
            create: true,
            dropdownDirection: 'up',
            options: {!! $tags !!}
        });
    </script>
@endsection