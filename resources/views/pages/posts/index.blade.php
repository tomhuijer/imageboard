@extends('master')

@section('title', 'Posts')

@section('content')
    <div class="row">
        <nav class="col-md-2">
            <form id="form-tags-search" class="form-inline" method="post">
                {{ csrf_field() }}
                <input class="form-control mr-2" type="search" placeholder="Search" aria-label="Search" name="query" value="{{ $search->query }}">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>

            <ul class="search-tags mt-4">
                @foreach ($tags as $tag)
                <li>
                    <a href="{{ route('posts.index', [$search->appendQueryString($tag->tag), 1]) }}">+</a>
                    <a href="{{ route('posts.index', [$search->appendQueryString('-' . $tag->tag), 1]) }}">-</a>
                    <a href="{{ route('posts.index', [$tag->tag, 1]) }}">{{ $tag->tag }}</a>
                    <small class="text-muted">{{ $tag->count }}</small>
                </li>
                @endforeach
            </ul>
        </nav>

        <main role="main" class="col-md-10 text-center">
            @forelse ($posts as $post)
                <div class="post-wrapper">
                    <a href="{{ route('posts.show', ['post' => $post->id]) }}" class="post-thumbnail">
                        <img src="{{ $post->thumbnail() }}" />
                        @if ($post->filetype->extension == 'gif')
                            <div class="post-label">gif</div>
                        @endif
                    </a>
                </div>
            @empty
                <p>No results.</p>
            @endforelse

            @include('partials.pagination')
        </main>
    </div>
@endsection
