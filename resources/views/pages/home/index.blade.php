@extends('master')

@section('content')
    <div class="row">
        <nav class="col-md-12 text-center">
            <form class="form-inline d-inline-block" method="post" action="{{ route('posts.index') }}">
                {{ csrf_field() }}
                <input class="form-control mr-2" type="search" placeholder="Search" aria-label="Search" name="query">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
    </div>
@endsection