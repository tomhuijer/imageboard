@extends('master')

@section('title', 'Upload')

@section('content')
    <div class="row">
        <nav class="col-md-12 text-center">
            <form class="form-inline d-inline-block" method="post" action="{{ route('upload.index') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="file" aria-label="File" name="file">
                <button class="btn btn-outline-success" type="submit">Upload</button>
            </form>
        </nav>
    </div>
@endsection