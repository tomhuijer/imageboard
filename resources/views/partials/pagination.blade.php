<nav class="mt-3">
    <ul class="pagination justify-content-center">
        <li class="page-item">
            @if ($pagination->currentPage == 1)
                <span class="page-link disabled" aria-label="First">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">First</span>
                </span>
            @else
                <a class="page-link" href="{{ route('posts.index', $search->isset() ? [$search->queryString(), 1] : [1]) }}" aria-label="First">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">First</span>
                </a>
            @endif
        </li>
        <li class="page-item">
            @if ($pagination->currentPage == 1)
                <span class="page-link disabled" aria-label="Previous">
                    <span aria-hidden="true">&lsaquo;</span>
                    <span class="sr-only">Previous</span>
                </span>
            @else
                <a class="page-link" href="{{ route('posts.index', $search->isset() ? [$search->queryString(), $pagination->previous()] : [$pagination->previous()]) }}" aria-label="Previous">
                    <span aria-hidden="true">&lsaquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            @endif
        </li>
        @foreach ($pagination->pages() as $page)
            @if ($pagination->currentPage == $page)
                <li class="page-item active">
                    <span class="page-link disabled">{{ $page }}</span>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="{{ route('posts.index', $search->isset() ? [$search->queryString(), $page] : [$page]) }}">
                        {{ $page }}
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
            @endif
        @endforeach
        <li class="page-item">
            @if ($pagination->currentPage == $pagination->totalPages)
                <span class="page-link disabled" aria-label="Next">
                    <span aria-hidden="true">&rsaquo;</span>
                    <span class="sr-only">Next</span>
                </span>
            @else
                <a class="page-link" href="{{ route('posts.index', $search->isset() ? [$search->queryString(), $pagination->next()] : [$pagination->next()]) }}" aria-label="Next">
                    <span aria-hidden="true">&rsaquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            @endif
        </li>
        <li class="page-item">
            @if ($pagination->currentPage == $pagination->totalPages)
                <span class="page-link disabled" aria-label="Last">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Last</span>
                </span>
            @else
                <a class="page-link" href="{{ route('posts.index', $search->isset() ? [$search->queryString(), $pagination->totalPages] : [$pagination->totalPages]) }}" aria-label="Last">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Last</span>
                </a>
            @endif
        </li>
    </ul>
</nav>