
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Dashboard</title>

    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dropdown
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">Disabled</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2">
            <form class="form-inline">
                <input class="form-control mr-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>

            <ul style="list-style: none; padding-left: 0; margin-top: 30px;">
                <li>
                    <a href="#">?</a>
                    <a href="#">+</a>
                    <a href="#">-</a>
                    <a href="#">lorem_ipsum</a>
                    <small class="text-muted">{{ rand(150, 6000) }}</small>
                </li>
                <li>
                    <a href="#">?</a>
                    <a href="#">+</a>
                    <a href="#">-</a>
                    <a href="#">dolor_sit</a>
                    <small class="text-muted">{{ rand(150, 6000) }}</small>
                </li>
                <li>
                    <a href="#">?</a>
                    <a href="#">+</a>
                    <a href="#">-</a>
                    <a href="#">amet_consectetur</a>
                    <small class="text-muted">{{ rand(150, 6000) }}</small>
                </li>
                <li>
                    <a href="#">?</a>
                    <a href="#">+</a>
                    <a href="#">-</a>
                    <a href="#">adipisicing_elit</a>
                    <small class="text-muted">{{ rand(150, 6000) }}</small>
                </li>
                <li>
                    <a href="#">?</a>
                    <a href="#">+</a>
                    <a href="#">-</a>
                    <a href="#">accusantium_voluptatem</a>
                    <small class="text-muted">{{ rand(150, 6000) }}</small>
                </li>
            </ul>
        </nav>

        <main role="main" class="col-md-10">
            @for ($i = 0; $i <= 30; $i++)
                <?php $j = rand(0, 1000) % 2 == 0; ?>
                <div style="max-width: 200px; max-height: 200px; display: inline-block; padding: 5px; margin: 10px; border: 1px solid #a2a2a2; border-radius: 3px;">
                    <img src="https://via.placeholder.com/{{ $j ? rand(90, 150) : 150 }}x{{ $j ? 150 : rand(90, 150) }}?text=%20" />
                </div>
            @endfor

            <nav aria-label="Page navigation example" style="margin-top: 30px;">
                <ul class="pagination justify-content-center">
                    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                </ul>
            </nav>
        </main>
    </div>
</div>

<script src="{{ elixir('js/app.js') }}"></script>
</body>
</html>
