<?php

namespace App\Http\Controllers;

use App\Libraries\Pagination;
use App\Libraries\Search;
use App\Post;
use App\PostTag;
use App\Tag;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index($query = '', $page = false)
    {
        if (is_numeric($query) && !$page) {
            $page = $query;
            $query = '';
        }

        $search = new Search($query);

        $postsPerPage = 28;
        $posts = collect([]);
        $tags = Tag::byTimesUsed();

        if ($search->isset()) {
            $requiredTags = Tag::fromArray($search->requiredTags());

            if (count($requiredTags) == count($search->requiredTags())) {
                $optionalTags = Tag::fromArray($search->optionalTags());
                $excludedTags = Tag::fromArray($search->excludedTags());

                $posts = Post::whereTags($requiredTags, $optionalTags, $excludedTags);
            }

            $tags = Tag::removeSearchTags($tags, $search);
        } else {
            $posts = Post::latest();
        }

        $pages = max(1, (int) ceil($posts->count() / $postsPerPage));
        $page = min($pages, max(1, intval($page)));

        $posts = $posts->slice(($page - 1) * $postsPerPage, $postsPerPage);

        $pagination = new Pagination($pages, $page, 7);

        return view('pages/posts/index')->with([
            'page' => $page,
            'pages' => $pages,
            'pagination' => $pagination,
            'posts' => $posts,
            'search' => $search,
            'tags' => $tags
        ]);
    }

    public function search(Request $request)
    {
        $parameters = [1];

        if ($request->input('query')) {
            $parameters = array_prepend($parameters, (new Search())->formatSearchQuery($request->input('query')));
        }

        return redirect()->route('posts.index', $parameters);
    }

    public function show(Post $post)
    {
        $post->load('filetype');
        $post->load('tags');

        $post->tags = Tag::countTimesUsed($post->tags);

        $tags = Tag::autocomplete($post->tags->pluck('tag'));

        return view('pages/posts/show')->with([
            'post' => $post,
            'tags' => $tags
        ]);
    }
}
