<?php

namespace App\Http\Controllers;

use App\Filetype;
use App\Http\Requests\UploadRequest;
use App\Post;
use App\PostTag;
use App\Tag;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class UploadController extends Controller
{
    public function index()
    {
        return view('pages/upload/index');
    }

    public function create(UploadRequest $request)
    {
        $file = $request->file('file');

        $contents = file_get_contents($file->getRealPath());
        $checksum = crc32($contents);

        $post = Post::where('checksum', $checksum)->first();

        if (!$post) {
            $mimeType = $file->getClientMimeType();
            $mimeChecksum = crc32($mimeType);
            $extension = $file->getClientOriginalExtension();

            $fileName = $checksum . '.' . $extension;
            $filePath = 'uploads/' . $mimeChecksum;
            $thumbnailPath = 'thumbnails/' . $mimeChecksum;

            $file->storeAs($filePath, $fileName);

            $thumbnailWidth = 150;
            $thumbnailHeight = 150;
            $thumbnail = Image::make($file);
            $thumbnail->width() > $thumbnail->height() ? $thumbnailHeight = null : $thumbnailWidth = null;
            $thumbnail->resize($thumbnailWidth, $thumbnailHeight, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put($thumbnailPath . '/' . $fileName, (string) $thumbnail->encode());

            $filetype = Filetype::firstOrCreate([
                'mimetype' => $mimeType,
                'extension' => $extension
            ]);

            $post = Post::create([
                'filetype_id' => $filetype->id,
                'checksum' => $checksum
            ]);

            if ($extension == 'gif') {
                $tag = Tag::firstOrCreate(['tag' => 'gif']);

                PostTag::create([
                    'post_id' => $post->id,
                    'tag_id' => $tag->id
                ]);
            }
        }

        return redirect()->route('posts.show', $post->id);
    }
}
