<?php

namespace App\Http\Controllers;

use App\Libraries\Pagination;
use App\Libraries\Search;
use App\Post;
use App\PostTag;
use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function create(Post $post, Request $request)
    {
        $post->load('tags');

        $tags = collect(explode(',', $request->input('tags')));

        $dbTags = Tag::whereIn('tag', $tags)->get();

        foreach ($tags as $tag) {
            $tag = str_slug($tag, '_');

            if (strlen($tag) < 1 || $post->tags->where('tag', $tag)->first()) {
                continue;
            }

            $dbTag = $dbTags->where('tag', $tag)->first();

            if (!$dbTag) {
                $dbTag = Tag::create([
                    'tag' => $tag
                ]);
            }

            PostTag::create([
                'post_id' => $post->id,
                'tag_id' => $dbTag->id
            ]);
        }

        return redirect()->route('posts.show', $post->id);
    }

    public function destroy(Post $post, Tag $tag)
    {
        PostTag::where([
            'post_id' => $post->id,
            'tag_id' => $tag->id
        ])->delete();

        if (PostTag::where('tag_id', $tag->id)->count() == 0) {
            $tag->delete();
        }

        return redirect()->route('posts.show', $post->id);
    }
}
