<?php

namespace App\Libraries;

class Search
{
    public $query;

    public $spaceCharText = ' ';
    public $spaceCharQuery = '+';
    public $optionalChar = '~';
    public $wildcardChar = '*';
    public $excludeChar = '-';

    protected $tags = false;
    protected $allTags = false;
    protected $requiredTags = false;
    protected $optionalTags = false;
    protected $wildcardTags = false;
    protected $excludedTags = false;

    public function __construct($query = '')
    {
        if (strlen($query) > 0) {
            $query = preg_replace($this->regex(), '', strtolower($query));
            $query = str_replace($this->spaceCharQuery, $this->spaceCharText, $query);
        }

        $this->query = $query;
    }

    public function isset()
    {
        return strlen($this->query) > 0;
    }

    public function regex($addition = '')
    {
        return '/[^a-z0-9' .
            '\\' . $this->spaceCharQuery .
            '\\' . $this->optionalChar .
            '\\' . $this->wildcardChar .
            '\\' . $this->excludeChar .
            '\_' . $addition . ']/';
    }

    public function queryString()
    {
        return str_replace([$this->spaceCharText], [$this->spaceCharQuery], $this->query);
    }

    public function formatSearchQuery($query)
    {
        $query = strtolower($query);
        $query = preg_replace($this->regex('\ '), '', $query);
        return str_replace($this->spaceCharText, $this->spaceCharQuery, $query);
    }

    public function appendQueryString($appendTag)
    {
        if ($this->isset()) {
            return $this->queryString() . $this->spaceCharQuery . $appendTag;
        }

        return $appendTag;
    }

    public function allTags()
    {
        if ($this->allTags) {
            return $this->allTags;
        }

        $this->allTags = $this->cleanTags($this->tags());
        return $this->allTags;
    }

    public function requiredTags()
    {
        if ($this->requiredTags) {
            return $this->requiredTags;
        }

        $this->requiredTags = array_where($this->tags(), function ($value) {
            return !starts_with($value, $this->optionalChar) && !starts_with($value, $this->excludeChar);
        });

        return $this->requiredTags;
    }

    public function optionalTags()
    {
        if ($this->optionalTags) {
            return $this->optionalTags;
        }

        $optionalTags = array_where($this->tags(), function ($value) {
            return starts_with($value, $this->optionalChar);
        });

        $this->optionalTags = $this->cleanTags($optionalTags);
        return $this->optionalTags;
    }

    public function wildcardTags()
    {
        if ($this->wildcardTags) {
            return $this->wildcardTags;
        }

        $wildcardTags = array_where($this->tags(), function ($value) {
            return str_contains($value, $this->wildcardChar);
        });

        $this->wildcardTags = $wildcardTags;
        return $this->wildcardTags;
    }

    public function excludedTags()
    {
        if ($this->excludedTags) {
            return $this->excludedTags;
        }

        $tags = [];

        foreach ($this->tags() as $tag) {
            if(starts_with($tag, $this->excludeChar)) {
                $tags[] = ltrim($tag, $this->excludeChar);
            }
        }

        $this->excludedTags = $tags;
        return $this->excludedTags;
    }

    protected function tags()
    {
        if ($this->tags) {
            return $this->tags;
        }

        if ($this->isset()) {
            $tags = array_where(explode($this->spaceCharText, $this->query), function ($value) {
                return strlen($value) > 0;
            });

            $this->tags = $tags;
            return $tags;
        }

        $this->tags = [];
        return [];
    }

    protected function cleanTags($tags)
    {
        foreach ($tags as &$tag) {
            $tag = ltrim($tag, $this->optionalChar);
            $tag = ltrim($tag, $this->excludeChar);
        }

        return $tags;
    }
}
