<?php

namespace App\Libraries;

class Pagination
{
    public $totalPages;
    public $currentPage;
    protected $size;

    public function __construct($totalPages = 1, $currentPage = 1, $size = 5)
    {
        $this->totalPages = $totalPages;
        $this->currentPage = $currentPage;
        $this->size = $size % 2 == 0 ? max(1, $size - 1) : $size;
    }

    protected function steps()
    {
        return (int) floor($this->size / 2);
    }

    protected function stepsFromStart()
    {
        return $this->currentPage - 1;
    }

    protected function stepsToEnd()
    {
        return $this->totalPages - $this->currentPage;
    }

    protected function minPage()
    {
        $steps = max($this->steps(), ($this->size - 1 - $this->stepsToEnd()));

        return max(1, $this->currentPage - $steps);
    }

    protected function maxPage()
    {
        $steps = max($this->steps(), ($this->size - 1 - $this->stepsFromStart()));

        return min($this->totalPages, $this->currentPage + $steps);
    }

    public function pages()
    {
        $pages = [];

        for ($i = $this->minPage(); $i <= $this->maxPage(); $i++) {
            $pages[] = $i;
        }

        return $pages;
    }

    public function previous()
    {
        return max(1, $this->currentPage - 1);
    }

    public function next()
    {
        return min($this->totalPages, $this->currentPage + 1);
    }
}
