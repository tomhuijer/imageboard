<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'filetype_id',
        'checksum'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function filetype()
    {
        return $this->belongsTo(Filetype::class);
    }

    public static function latest()
    {
        return Post::orderBy('created_at', 'desc')->orderBy('id', 'desc')->with('filetype')->get();
    }

    public static function whereTags($requiredTags, $optionalTags = [], $excludedTags = [])
    {
        if ((count($requiredTags) + count($optionalTags) + count($excludedTags)) > 0) {
            $query = Post::query();

            if (count($requiredTags) > 0) {
                $query = $query->where(function ($requiredQuery) use ($requiredTags) {
                    foreach ($requiredTags as $requiredTag) {
                        $requiredQuery->whereHas('tags', function ($tags) use ($requiredTag) {
                            $tags->where('id', $requiredTag->id);
                        });
                    }
                });
            }

            if (count($optionalTags) > 0) {
                $query = $query->orWhere(function ($optionalQuery) use ($optionalTags) {
                    foreach ($optionalTags as $optionalTag) {
                        $optionalQuery->orWhereHas('tags', function ($tags) use ($optionalTag) {
                            $tags->where('id', $optionalTag->id);
                        });
                    }
                });
            }

            if (count($excludedTags) > 0) {
                $query = $query->where(function ($excludedQuery) use ($excludedTags) {
                    foreach ($excludedTags as $excludedTag) {
                        $excludedQuery->whereDoesntHave('tags', function ($tags) use ($excludedTag) {
                            $tags->where('id', $excludedTag->id);
                        });
                    }
                });
            }

            return $query->orderBy('created_at', 'desc')->orderBy('id', 'desc')->with('filetype')->get();
        }

        return collect([]);
    }

    public function filename()
    {
        return $this->checksum . '.' . $this->filetype->extension;
    }

    public function thumbnail()
    {
        return asset('storage/thumbnails/' . crc32($this->filetype->mimetype) . '/' . $this->filename());
    }

    public function image()
    {
        return asset('storage/uploads/' . crc32($this->filetype->mimetype) . '/' . $this->filename());
    }
}
