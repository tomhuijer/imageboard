<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filetype extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mimetype',
        'extension'
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
