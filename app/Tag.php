<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tag'
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    public static function fromArray($tags)
    {
        if (count($tags) > 0) {
            return Tag::where(function ($query) use ($tags) {
                foreach ($tags as $tag) {
                    if (str_contains($tag, '*')) {
                        $query->orWhere('tag', 'like', str_replace('*', '%', $tag));
                    } else {
                        $query->orWhere('tag', '=', $tag);
                    }
                }
            })->get();
        }

        return collect([]);
    }

    public static function countTimesUsed($tags)
    {
        return $tags->each(function ($tag) {
            $tag->count = $tag->posts->count();
        });
    }

    public static function byTimesUsed()
    {
        return Tag::with('posts')
            ->get()
            ->sortBy(function ($tag) {
                $tag->count = $tag->posts->count();
                return $tag->count;
            })->forget('posts')->reverse();
    }

    public static function removeSearchTags($tags, $search)
    {
        return $tags->filter(function ($tag) use ($search) {
            $filteredTag = !in_array($tag->tag, $search->allTags());

            if ($filteredTag && count($search->wildcardTags()) > 0) {
                foreach ($search->wildcardTags() as $wildcardTag) {
                    if (fnmatch($wildcardTag, $tag->tag)) {
                        return false;
                    }
                }
            }

            return $filteredTag;
        });
    }

    public static function autocomplete($exclude = [])
    {
        $tags = Tag::whereNotIn('tag', $exclude)
            ->get(['tag AS value'])
            ->each(function ($tag) {
                $tag->text = $tag->value;
            });

        return json_encode($tags);
    }
}
