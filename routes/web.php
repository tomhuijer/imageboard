<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home.index');

Route::get('posts/{query?}/{page?}', 'PostController@index')->name('posts.index');
Route::post('posts/{query?}/{page?}', 'PostController@search');

Route::get('post/{post}', 'PostController@show')->name('posts.show');
Route::post('post/{post}', 'TagController@create')->name('tags.create');
Route::get('post/{post}/{tag}', 'TagController@destroy')->name('tags.destroy');

Route::get('upload', 'UploadController@index')->name('upload.index');
Route::post('upload', 'UploadController@create');
